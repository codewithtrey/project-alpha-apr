from django.urls import path
from projects.views import list_projects, show_project, create_project, search_feature, view_timeline


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("search/", search_feature, name="search-view"),
    path("<int:id>/timeline/", view_timeline, name="view_timeline"),
]
